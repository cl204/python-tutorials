"""
In this tutorial we look at array-like data-types and how they can be used to good effect for various applications
including table storage and matrix-operations.

In general, for simple data storage, built-in types such as dictionaries, lists and tuples are often the fastest,
lowest cost and simplest to use, but are limited in scope if doing advanced operations. Pandas dataframes offer a
convenient excel/sql-like object in which it is possible to store large amounts of data stored in table format,
with column headings. NumPy arrays offer a powerful scientific array-like structure, most similar to Matlab arrays in
construction and functionality.

numpy --> https://numpy.org/doc/stable/index.html
pandas --> https://pandas.pydata.org/docs/user_guide/index.html
networkx --> https://networkx.github.io/documentation/stable/index.html
"""


from datetime import date
import pandas as pd
import numpy as np
import networkx as nx


def dict_as_array(team):
    """
    As seen previously, dicts offer a convenient way to store data that's organised according to a key-word and quick
    to search through as the data's location in memory is known. Here, the key-words are used as column headings,
    with their values (lists) representing the row entries in those columns.

    One potential issue with dicts is because of their flexibility, there's nothing forcing the list lengths to be
    equal, so it's not a great representation of an array.

    Also, they are not set up to easily carry out matrix mathematics, so much better suited to simple data entry and
    extraction.
    """

    print("------ Dictionaries -------")

    # direct invocation of dict
    print("Alex's job is", team['job'][0])

    print("")

    # parametric invocation of dict based on the names
    for n in team['name']:
        print(
            n,
            "started work on",
            [team['start date'][idx] for idx in range(len(team['name'])) if team['name'][idx] == n][0]
        )

    print("")


def numpy_arrays():
    print("------ Numpy Arrays -------")
    # Numpy Array construction is similar to list construction
    my_list = list([
        [1, 2, 3],
        [4, 5, 6]
    ])

    my_array = np.array([
        [1, 2, 3],
        [4, 5, 6]
    ])

    sequence_array = np.linspace(1, 2, 10)

    b = np.array([
        [1, 1],
        [0, 1]
    ])

    c = np.array([
        [2, 0],
        [3, 4]
    ])

    print(b*c)  # element-wise product
    print(b@c)  # matrix product
    print(b.dot(c))  # also matrix product

    print(c.T)  # Transpose

    # Logical (boolean) indexing allows us to do matrix-wide operations based on specific characteristics of the
    # entries. For example, here we create a boolean matrix showing where there are values in the main matrix greater
    # than 4, and then use this bool matrix to modify the original matrix in some way
    d = np.arange(12).reshape(3, 4)
    print(d)
    e = d > 4
    d[e] = 0
    print(d)
    print("")


def pandas_dataframes(team, nodes, a):
    print("------ Pandas DataFrames -------")

    # Create a dataframe using the dict object
    team_df = pd.DataFrame(data=team)

    print(team_df)

    names = team['name']
    other_data = dict(
        (k, team[k]) for k in ('job', 'start date', 'email')
    )

    team_df_alt = pd.DataFrame(
        other_data,
        index=team['name']
    )

    # Select elements from the table using "loc"
    print(team_df_alt.loc['Robin', 'email'])

    # Select elements from the table using the index ("iloc)
    print(team_df_alt.iloc[2][2])
    pass

    # Create a dataframe based on a numpy array object, with custom row and column labels
    adj_df = pd.DataFrame(
        a,  # data to go into the main table body
        nodes,  # row labels
        nodes,  # col labels
        dtype=np.int8
    )

    adj_df_sq = adj_df * adj_df
    adj_df_gt2 = adj_df > 2

    # The following would not work, since the data types in the team_df object are not suitable for a "greater than"
    # operation like this.
    # team_df_gt2 = team_df > 2
    print("")


def networkx_graphs(nodes, edges):
    print("------ NetworkX Graphs -------")

    g = nx.Graph()
    g.add_nodes_from(nodes)

    g.add_weighted_edges_from(edges)

    betweenness = nx.betweenness_centrality(g)
    print("Betweenness =", betweenness)
    print("")


if __name__ == "__main__":
    team = {
        'name': [
            'Alex',
            'Chris',
            'Robin'
        ],
        'job': [
            'dev',
            'admin',
            'analyst'
        ],
        'start date': [
            date(2010, 1, 1),
            date(2012, 4, 25),
            date(2019, 7, 2)
        ],
        'email': [
            'alex@work.com',
            'chris@work.com',
            'robin@work.com'
        ]
    }

    # Use the above dictionary as a kind of table, where the keys (name, job...etc) are the column headings and the
    # values for each of those headings are the row entries
    dict_as_array(team)

    numpy_arrays()

    nodes = ('a', 'b', 'c', 'd', 'e')
    adj = np.random.randint(
        0,
        6,
        [len(nodes), len(nodes)]
    )

    pandas_dataframes(team, nodes, adj)

    edges = [
        ('a', 'c', 1),
        ('b', 'c', 4),
        ('d', 'b', 2),
        ('c', 'd', 3),
        ('a', 'e', 1)
    ]

    networkx_graphs(nodes, edges)

    pass
