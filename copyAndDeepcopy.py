__author__ = "Christopher Lowe"


"""
Module to look at the important differences between referencing and copying :)

In python, associating an object with another object creates a "reference", rather than a copy.
The two will therefore be linked by default, but it doesn't always appear so.
The key is *mutability*

Immutable (built-in) types:
- Booleans
- Integers
- Floats
- Strings
- Tuples

Mutable (built-in) types:
- Lists
- Sets
- Dictionaries

"""


import copy


def edit_mutable(x, y):
    print_results(x, y, 'before')
    x[1] = 4
    print_results(x, y, 'after')
    print('-------------------------')


def reassign_immutable(x, y):
    print_results(x, y, 'before')
    x = 'hi'
    print_results(x, y, 'after')
    print('-------------------------')


def print_results(x, y, when):
    print('"a" is', x, when, 'change')
    print('"b" is', y, when, 'change')


a = [1, 2, 3]
b = a
edit_mutable(a, b)

c = 'hello'
d = c
reassign_immutable(c, d)

a = [1, 2, 3]
# A "deep copy" basically goes into the structure of the object being copied, until it gets to the immutable level, and
#   reconstructs a new object using this data, rather than creating a "reference" to the original object
b = copy.deepcopy(a)
edit_mutable(a, b)
