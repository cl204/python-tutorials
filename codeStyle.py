"""
In this module, we look at how to lay out our code and why it is good to do things in
certain ways. Code style is, to a large extent, personal preference, however if working
in a team, then it's often considered more efficient and effective to be consistent.

Python Style Guide "PEP 8" --> https://www.python.org/dev/peps/pep-0008/
Python Docstring Conventions "PEP 257" --> https://www.python.org/dev/peps/pep-0257/
"""

# "Dunder" names (names with double leading/trailing underscores) go before the imports
__author__ = "Christopher Lowe"
__version__ = 1.0


# Be explicit with what you're importing, the following are "good"
import numpy as np
from math import pi, radians, cos

# ...but this is dangerous
# from pandas import *


# The widely accepted line length is 90 characters -------------------------------------->
# Any less and you quickly run out of space when doing nested loops within classes etc,
# while any more and you don't get much benefit in terms of shorter scripts


def indentation(a, b, c):
    """Illustrating the impact of indentation

    The following stuff in this docstring is just an example of something you might want
    do, ultimately this should be tailored to the problem at hand. E.g. if it's
    absolutely vital that a parameter is of a particular type and in a particular
    format, then it's good practice to add that in the section below, so that a user of
    the function knows how to provide the input

    :param a: This is the first argument
    :type a: int
    :param b: This is the second arg
    :type b: str
    :param c: (tuple) This is the third arg, with the "type" included in the param line
    :type c: tuple

    :return dict_var:
    """
    # This is fine for variables/functions that are relatively small and will always
    # remain small, no matter what
    dict_var = {a: "hey", b: ["hello", 3, "times"], c: "hi there"}

    # This is done a lot, and has issues with it, e.g. if we refactored the variable 'b'
    # to something longer or changed the dict name itself, things would get out of line
    dict_var = {a: "hey",
                b: ["hello",
                    3,
                    "times"],
                c: "hi there"}

    # This is how it "should" be done, and while it takes up more space and for simple
    # things may look more complex, it is more robust to changes and maintains clarity
    # when things start to get messy :)
    dict_var = {
        a: "hey",
        b: [
            "hello",
            3,
            "times"
        ],
        c: "hi there"
    }

    return dict_var


def square_func(k):
    """Method that returns the square of the input argument
    :param k: variable to be squared
    :type k: float, int
    :return: Square of the input argument
    """
    if not isinstance(k, (float, int)):
        raise TypeError(k, "is not of the correct type")

    # Square the input argument
    k_squared = k ** 2

    # Return the squared number
    return k_squared


def square_func_better(x):
    return x**2


def non_pythonic_way(x):
    """ This code is technically correct, returning a dict of all the even numbers (
    keys) and their squared value (value)
    """
    d = {}
    idx = -1
    for k in x:
        idx += 1
        if k % 2 == 0:
            d[idx] = k**2
    return d


def pythonic_way(x):
    """ This code makes better use of python, exploiting the built in capabilities
    while maintaining readability. It does exactly the same thing, but is "cleaner"
    """
    return {idx: k**2 for idx, k in enumerate(x) if not k % 2}


# Notice the 2 blank lines between module-level classes/functions
if __name__ == "__main__":

    response = indentation(1, 2, 3)
    # response = indentation(1, "two", (3,))

    x = [k for k in range(10, 100)]
    non_py = non_pythonic_way(x)
    py = pythonic_way(x)
    pass
