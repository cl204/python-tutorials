__author__ = "Christopher Lowe"


class Satellite:
    """
    This is the "base" class for a generic satellite
    """
    def __init__(self, name, tx_rate):
        self.name = name
        self.tx = tx_rate  # rate of data download (bps)
        self.ob_data = 0.  # data stored on board
        self.delivered_data_vol = 0  # variable to show the volume of delivered data

    def acquire_data(self, vol_packet, num_packets):
        """
        Acquire payload data
        :param vol_packet: (float) volume of a packet of data (bits)
        :param num_packets: (integer) number of packets being acquired
        :return:
        """
        self.ob_data += num_packets * vol_packet

    def send_data(self, t_pass):
        """
        Method to download data
        :param t_pass: (float) time of ground station pass (seconds)
        :return:
        """
        data0 = self.ob_data

        pass_capacity = t_pass * self.tx

        self.ob_data = max(data0 - pass_capacity,
                           0.)

        self.delivered_data_vol = data0 - self.ob_data


class Cubesat(Satellite):
    """
    CubeSat class, which is a "child" of the Satellite class
    """
    def __init__(self, name, tx, u):
        """
        Note the additional input argument of "u", referring to the U-volume of the CubeSat, not applicable to Satellite
        """
        super().__init__(name, tx)  # super method is what assigns all of the attributes and methods from Satellite
        self.u = u
        x = 5
        self.y = x**2


# Module-level method
def sum_all(x):
    """ method to sum all of the values in the list "x"
    """
    total = 0

    for num in x:
        total += num

    return total


def main():
    # instantiations of Satellite class
    sat1 = Satellite('my 1st satellite', 10.)
    sat2 = Satellite('my 2nd satellite', 20.)

    # instantiation of CubeSat class
    sat3 = Cubesat('my 1st CubeSat', 5., 3)

    sats = [sat1, sat2, sat3]  # a list containing all of the satellites

    vol_packets = 10.
    n_packets = 50.
    sat1.acquire_data(vol_packets, n_packets)

    sat2.acquire_data(200., 35)
    sat3.acquire_data(5., 1000)

    gs_pass = 60*5

    for s in sats:
        print(s.name, 'has', s.ob_data, ' bits on board before the pass')
        s.send_data(gs_pass)
        print(s.name, 'has', s.ob_data, 'bits remaining on board after the pass')

    total_delivered = sum_all([s.delivered_data_vol for s in sats])

    print('the total volume of delivered data is', total_delivered)


if __name__ == "__main__":
    main()
