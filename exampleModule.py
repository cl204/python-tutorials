"""Python module to be referenced as example of "good practice"

This module is to be used as a reference point for how to implement good coding practices.
In terms of "style" the majority of what's here is taken from https://www.python.org/dev/peps/pep-0008/
Docstring conventions can be found in https://www.python.org/dev/peps/pep-0257/
"""


# __all__ = ???  # specific variables that are to be imported to another module if "from exampleModule import *" is
#   called in the import statements of the other module. If this is left commented out, everything gets imported
__author__ = 'FirstName LastName'
__version__ = 1.0


# imports go here, for example:
import math  # example of importing the whole module. If using a method/class from this module, need "math.xyz"
from collections import deque  # example if importing a specific class, here only "deque" is required to be typed


class BaseClass:
    """
    Class at the top of a hierarchy tree, which may/may not be inherited by other classes
    """
    class_attr = 123  # "class variable" (shared by all instances of this class). Should only be used if ALWAYS the same

    def __init__(self, a, b=10):
        """
        Initialisation method invoked automatically whenever this class is "instantiated"
        :param a:
        :param b:
        """
        self.a = a  # "instance variable", unique to each object
        self.b = b

        c = a**2  # "local variable", cannot be called outside of this method.
        self.a_sq = c

    # Always leave 1 empty line between class/method-level methods
    def loop_example(self):
        pass

    @staticmethod
    def base_class_static_method():
        pass


# Always leave 2 empty lines between module-level classes/methods
class ChildClass(BaseClass):
    def __init__(self, a, b, name='somebody'):
        """
        Child class that inherits attributes and methods from the Base class
        :param a:
        :param b:
        :param name: Name (String)
        """
        # the "super()" method effectively invoked the Parent class' "__init__()" method, and as such inherits the
        #   attributes and methods from that class such that they're available in this class
        super().__init__(a, b)
        self.name = name  # instance variable that is not an attribute of the Base class

    def say_hello(self):
        print("Hello, my name is", self.c)


def module_level_method(arg1, arg2, *args, **kwargs):
    pass
