__author__ = "Christopher Lowe"


def boolean_type():
    """
    Method to illustrate booleans and their uses.
    Booleans are used to indicate whether a condition is, or is not, met
    :return:
    """
    a = True
    b = False

    if a:
        print('a is true')

    if not b:
        print('b is false')

    c = 5
    d = c > 4  #

    e = d == c > 4  # False, because it looks at the "==" first and then the ">"
    f = d == (c > 4)  # True :)

    if c > 4:  # What this is doing is, as above, it's asking "if this statement = "True", do something
        print('hello')

    my_list = [1, 2, 3, 4, 5]
    g = 3 in my_list

    return


def integer_type():
    a = 1 + 2  # returns an int
    b = 1 + 2.5  # returns a float
    c = 15 / 4  # returns 3.75 in Python 3, but would return 3 in Python 2 - BE CAREFUL!
    d = 6 / 3  # float, even though everything involved could be an integer
    return


def float_type():
    a = 1.25 - 1.0
    if a == 0.25:
        print('a is 0.25')
    else:
        print('a is not 0.25')

    b = 1.2 - 1.0
    if b == 0.2:
        print('b is 0.2')
    else:
        print('b is not 0.2')

    return


def string_type():
    """
    Strings are useful for capturing text, but they're quite powerful in terms of also being able to be operated on
    """
    my_str = 'hello my friends, my name is Chris'
    my_str2 = "hello my friends, my name is Chris also"
    my_str3 = str("hello my friends, my name is Chris three")

    print(my_str.split())
    print(my_str.split(','))
    print(my_str.split('i'))

    print(my_str + ' and I am ' + str(25) + ' years old')

    x = int('8')

    return


def tuple_type():
    """
    Tuples look like, but are not, lists with curly brackets instead of square brackets... You can't "do" a whole lot
    with tuples, and that's deliberate because they are designed to store things that don't change. As such, tuples are
    IMMUTABLE, which means they are secure
    :return:
    """
    my_tuple = (1, 2, 3)

    my_list = [3, 4, 5]
    tuple_from_list = tuple(my_list)

    return


def list_type():
    my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    # indexing
    print(my_list[0])

    # loop by iterating over the list entries
    for k in my_list:
        print(k)

    # 3 x 3 array-like object as 3 lists within a list
    array_like_list = [[1, 2, 3],
                       [2, 3, 4],
                       ['hi', 'hello', 'hey']
                       ]

    # List comprehension allows us to create lists based on other iterable objects and conditions
    even_list_from_list = [x for x in my_list if not x % 2]  # creates a list of the even numbers
    print(even_list_from_list)

    return


def set_type():
    """
    Sets are unordered collections of unique items. The are "hashed", such that the location of each item is "known",
    and they are mutable such that you can add/remove/edit elements

    It's easy to mistake a set as a list with no repeated items, however this is not the case. Better is to think of a
    set as a dict, but only with keys (no values)

    Sets are not indexed, so asking for the "nth" element in a set does not work.
    :return:
    """
    my_list = [1, 2, 2, 3, 4, 5, 5, 5]
    my_set = set(my_list)
    print(my_set)

    my_other_set = {5, 6, 7}
    my_union_set = my_set.union(my_other_set)
    my_intersection_set = my_set.intersection(my_other_set)

    return


def dict_type():
    """"
    Dictionaries contain a set of "keyword-value" pairs, which are useful in a number of applications:
        - when order of the items does not matter
        - when you need to locate items (the entries are "hashed", which means finding something is very fast - kind of
            like built-in indexing)
        - when there's an obvious association between attributes, e.g. the keyword could be a unique ID, with the value
            being the associated object.

    Things to note:
        - in Python 2 dicts are unordered (i.e. if you iterated over the dict items in a loop, they could arrive in any
            order). In Python 3, however, items will be extracted in the order they were added to the dict.
        - No two keywords can be the same in one dict
        - Keywords must be immutable objects (int, float, boolean, tuple, string). Lists, dicts and sets are not valid
        - Values can be ANYTHING and can be duplicated
    """
    my_dict = {'one': 1,
               'two': 2.0,
               'three': (3, 3, 3),
               'four': 'hello',
               5: [5, 5, 5],
               6.0: {6.1: 'hi',
                     6.2: 'hello',
                     'six point 3': 6.3
                     },
               tuple([7, 'seven']): 7
               }

    print(my_dict[5])
    print(my_dict[6.0]['six point 3'])

    my_list_from_dict_keys = [x for x in my_dict]
    print(my_list_from_dict_keys)

    my_list_from_dict_values = [x for x in my_dict.values()]
    print(my_list_from_dict_values)

    return


if __name__ == "__main__":

    boolean_type()
    integer_type()
    float_type()
    string_type()
    tuple_type()
    list_type()
    set_type()
    dict_type()

    person1 = {'name': 'Alex',
               'age': 25,
               'height': 1.5,
               'id': 123
               }

    person2 = {'name': 'Lindsay',
               'age': 40,
               'height': 1.75,
               'id': 285
               }

    person3 = {'name': 'Lou',
               'age': 27,
               'height': 1.9,
               'id': 444
               }

    # create a list containing each person
    person_list = [person1,
                   person2,
                   person3
                   ]

    # create a dict containing each person ID (Keyword) and it's associate "person" object (attribute)
    person_dict = {x['id']: x for x in person_list}

    # TASK: Add the attribute "Job: Teacher" to the person with ID #444

    person_dict[444]['job'] = "Teacher"

    for p in person_list:
        if p['id'] == 444:
            p['job'] = 'Teacher'
            break


    pass