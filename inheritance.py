__author__ = "Christopher Lowe"


"""
Most of this code below is taken from https://realpython.com/python-super/#super-in-single-inheritance
"""


class Rectangle:

    def __init__(self, length, width):
        self.length = length
        self.width = width

    def area(self):
        return self.length * self.width

    def perimeter(self):
        return 2 * self.length + 2 * self.width


class SquareBasic:
    """
    We're not inheriting anything from Rectangle, so we have to do all of the methods etc from scratch... urghhh!
    """
    def __init__(self, length):
        self.length = length

    def area(self):
        return self.length * self.length

    def perimeter(self):
        return 4 * self.length


class SquareInherit(Rectangle):
    """
    A square is really just a special case of Rectangle, so we can inherit the properties and make use of them :)

    Here we are inheriting everything from Rectangle, and not specifying anything at all for square. Not ideal if there
    are subtle differences and kind of pointless as you would just instantiate Rectangle instead...
    """
    pass


class SquareInheritBetter(Rectangle):
    """
    Here were are inheriting the attributes and methods from Rectangle, but we've defined our own constructor for
    Square, since we only need one length param, not two.
    """
    def __init__(self, length):
        Rectangle.__init__(self, length, length)


class SquareSuper(Rectangle):
    """
    This is the same as above, but is more "general", so preferred, since we're not calling the Rectangle class in the
    constructor __init__().
    """
    def __init__(self, length):
        super().__init__(length, length)

    # def area(self):
    # # This over-rides the area() method in the super class
    #     print('This is the area from the Square class')
    #     return self.length ** 2


class Cube(SquareSuper):
    """

    """
    def area(self):
        face_area = super().area()
        return face_area * 6

    def volume(self):
        face_area = super().area()
        return face_area * self.length


def main():

    rectangle = Rectangle(3, 4)
    print('rectangle area is', rectangle.area())
    print('rectangle perimeter is', rectangle.perimeter())
    print('-------------------------------------')

    square_basic = SquareBasic(3)
    print('square_basic area is', square_basic.area())
    print('square_basic perimeter is', square_basic.perimeter())
    print('-------------------------------------')

    square_inherit = SquareInherit(4, 4)
    print('square_inherit area is', square_inherit.area())
    print('square_inherit perimeter is', square_inherit.perimeter())
    print('-------------------------------------')

    square_inherit_better = SquareInheritBetter(5)
    print('square_inherit_better area is', square_inherit_better.area())
    print('square_inherit_better perimeter is', square_inherit_better.perimeter())
    print('-------------------------------------')

    square_super = SquareSuper(6)
    print('square_super area is', square_super.area())
    print('square_super perimeter is', square_super.perimeter())
    print('-------------------------------------')

    cube = Cube(3)
    print('cube surface area is', cube.area())
    print('cube volume is', cube.volume())
    print('cube perimeter is', cube.perimeter(), ', ummmmm......')


main()
