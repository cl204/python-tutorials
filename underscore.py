"""
This module looks into how the underscore is used in Python. This is not an exhaustive overview, as there are other
uses based primarily around code clarity and best practice, but here are some common applications :)
"""

author = "Christopher Lowe"


def underscore_ignore():
    # Using underscores to ignore certain parts of a response
    x, _, _, y = ('hello', 'hey', 'hi', 'hiya')
    return x, y, _


class SingleLeadingVar:
    def __init__(self):
        self.foo = 11
        self._bar = 23


def _single_leading_method():
    print("I didn't work")


class ManglingTest:
    def __init__(self):
        # This is "protected" variable, that we cannot access from outside of the class itself
        self.__var = 'protected var'

    def get_var(self):
        # This method allows us to gain access to our protected variable
        return self.__var

    def set_var(self, x):
        # This method allows us to edit our protected variable
        self.__var = x

    def __dunder_mangled(self):
        # This is a protected method, we cannot access this directly from outside
        print("You're about to get the protected variable...")
        print(self.__var)

    def call_dunder_mangled(self):
        # This method allows us to invoke our protected method from outside the class
        self.__dunder_mangled()


class ExtManglingTest(ManglingTest):
    def __init__(self):
        super().__init__()
        self.__var = 'hey'


# declare our own reverse class that does things backwards...
class ReverseInt(int):
    # magic __init__ method to initiate object
    def __init__(self, var):
        super().__init__()
        self.__value = var

    # "new" add method, so that we can add two strings together
    def __add__(self, x):
        return self.__value - x

    def __sub__(self, x):
        return self.__value + x


if __name__ == "__main__":

    # Using underscores to ignore things
    a, b, c = underscore_ignore()

    # ------------------------------------------------------------------------------------------------------------------

    # Effect of the single leading underscore is minimal. Nothing changes from the POV of using the code, but it
    #   "implies" that the object is private and should not be accessed directly
    s = SingleLeadingVar()

    # ------------------------------------------------------------------------------------------------------------------

    # The double-leading underscore ("dunder") has a great impact as it actually changes the name of the variable in
    #   terms of how we access it. It "mangles" the name such that it's an absolute pain to access, which is a
    #   protection mechanism, in particular where inheritance is used
    m = ManglingTest()
    em = ExtManglingTest()

    # ------------------------------------------------------------------------------------------------------------------

    # Double leading AND trailing underscores on methods indicate "magic" methods. You tend to find these as built-in
    #   methods, but it is possible to over-ride these and modify them as required. In fact, we do this most of the time
    #   we instantiate a bespoke class, with the __init__() method. This built-in method is executed every time we
    #   create an object, but for custom classes we most likely want to add in our own parameters etc, so we modify it
    my_list = [1, 2, 3, 4, 5]
    print('the length of my list is', len(my_list))
    print('the length of my list is also', my_list.__len__())

    # Create a (rather pointless) "special integer" class that reverses the add (__add__) and subtract (__sub__)
    #   magic methods so that it behaves all funny...
    r = ReverseInt(10)
    print('For my special int class, 10 + 3 = ', r + 3)
    print('For my special int class, 10 - 2 = ', r - 2)
    pass