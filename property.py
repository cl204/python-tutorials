"""
This module demonstrates the benefits of using the @property decorator to isolate variables and methods from the outside
world through the use of getters and setters

Much of the code here is taken (and adapted) from https://www.programiz.com/python-programming/property
"""

author = "Christopher Lowe"


class CelsiusBasic:
    """
    Basic class representing the temperature

    We simply use obj_name.t to get the temperature :) easy. The issue is that if at any point this class changes so
    that obj_name.t is not enough to get the temperature, all the code using this is out of date
    """
    def __init__(self, temperature=0):
        self.t = temperature


# Making Getters and Setter methods
class CelsiusGetterSetter:
    """
    In this class, rather than assign the input argument to a class variable "temperature", we set it using a
    "setter" method and access it using a "getter" method. This is much more robust as we can now put whatever we like
    into the setter and getter methods, knowing we'll get the temperature out the other side. It is, however, not a
    very elegant way to get a variable. It's much nicer to just get it edit it directly, rather than through a method.

    We're using a "private" variable (_t), with a single leading underscore, to highlight the fact that it should not be
    accessed directly
    """
    def __init__(self, temperature=0):
        self.set_t(temperature)

    # getter method
    def get_t(self):
        return self._t

    # setter method
    def set_t(self, value):
        if value < -273.15:
            raise ValueError("Temperature below -273.15 is not possible.")
        self._t = value


# Using @property decorator with a single-underscore attribute
class CelsiusPropertyUnder:
    """
    Here, we're using the very Pythonic @property decorator to allow us to get/set our variable directly, just like we
    can in the CelsiusBasic class, however we're not really accessing it directly as the @property decorator re-directs
    us to the get and set methods automatically.

    Again, we're using the single leading underscore to denote _t as a private variable. From the user's perspective,
    they just use self.t as any other variable and all seems normal from the outside, but we've wrapped it up, or
    "decorated" it with our getter and setter methods :)
    """
    def __init__(self, temperature=0):
        self.t = temperature

    @property
    def t(self):
        return self._t

    @t.setter
    def t(self, value):
        if value < -273.15:
            raise ValueError("Temperature below -273 is not possible")
        self._t = value


# Using @property decorator with a double-underscore attribute
class CelsiusPropertyDunder:
    """
    Identical to the above class, however we're using a dunder variable (__t), which not only tells us it's private, but
    protects it against being modified directly, in case someone gets a bit trigger happy. If used correctly, the
    behaviour of this class is the same as above, however if someone were to try and overwrite obj_name.__t, it would
    simply create a new variable with that name, rather than actually change the existing one, which has had its name
    mangled.
    """
    def __init__(self, temperature=0):
        self.t = temperature

    @property
    def t(self):
        return self.__t

    @t.setter
    def t(self, value):
        if value < -273.15:
            raise ValueError("Temperature below -273 is not possible")
        self.__t = value


if __name__ == "__main__":
    # Simple class
    c_basic = CelsiusBasic(10)

    # Basic use of getters and setters
    c_get_set = CelsiusGetterSetter(20)
    print(c_get_set.get_t())

    # Using the @property, but with a single underscore attribute
    c_under = CelsiusPropertyUnder(30)
    print(c_under.t)
    c_under.t = -50
    c_under._t = -1000

    # Using the @property, but with a double underscore attribute
    c_dunder = CelsiusPropertyDunder(40)
    print(c_dunder.t)
    c_dunder.t = -100
    c_dunder.__t = 'hello world'
    print(c_dunder.t)
    c_dunder.t = -200

    pass
